package com.binar;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        /*
        List<String> perabotanKosan = new ArrayList<>();
        perabotanKosan.add("meja"); // arr[0]
        perabotanKosan.add("lemari"); // arr[1]
        perabotanKosan.add("kasur"); // arr[2]
        for (String perabotan : perabotanKosan) {
            System.out.println(perabotan + " di indeks ke " + perabotanKosan.indexOf(perabotan));
        }
        System.out.println("----------------------");
        perabotanKosan.remove("lemari");
        for (String perabotan : perabotanKosan) {
            System.out.println(perabotan + " di indeks ke " + perabotanKosan.indexOf(perabotan));
        }
        System.out.println("==========================================");
        List<String> perabotanKosanSebelah = new ArrayList<>();
        perabotanKosanSebelah.addAll(perabotanKosan);
        perabotanKosanSebelah.add("PS 5");
        perabotanKosanSebelah.add("TV");
        for (String perabotan : perabotanKosanSebelah) {
            System.out.println(perabotan + " di indeks ke " + perabotanKosan.indexOf(perabotan));
        }

        List<List<String>> kosanCeria = new ArrayList<>();
        kosanCeria.add(perabotanKosan); // 0
        kosanCeria.add(perabotanKosanSebelah); // 1

        for (String perabotan : kosanCeria.get(1)) {
            System.out.println(perabotan);
        }
        */
        // List<String> blabla
        /*
        ---------------
        ---------------
        ---------------
         */

        // List<List<String>> blabla
        /*
        [------------------] 0
        [------------------] 1
        [------------------] 2
         */

        // Siswa yg belum bayar iuran
        /*
        Januari : 1. Rizky
                   2. Fauzi
                   3. ..

        Februari : 1. Fauzi
                   2. ...

        Maret : 1. Rizky
                2. Imam
                3. ...

        YANG BELUM BAYAR
        1. Rizky
        2. Fauzi
        5. Imam
        REMIND!!1!
         */
        /*
        Set<String> siswaNotIuranJan = new HashSet<>();
        siswaNotIuranJan.add("Rizky");
        siswaNotIuranJan.add("Fauzi");

        Set<String> siswaNotIuranFeb = new HashSet<>();
        siswaNotIuranFeb.add("Fauzi");

        Set<String> siswaNotIuranMar = new HashSet<>();
        siswaNotIuranMar.add("Rizky");
        siswaNotIuranMar.add("Imam");

        Set<String> ygBelumBayar = new HashSet<>();
        ygBelumBayar.addAll(siswaNotIuranJan);
        ygBelumBayar.addAll(siswaNotIuranFeb);
        ygBelumBayar.addAll(siswaNotIuranMar);

        Iterator<String> iterator = ygBelumBayar.iterator();

        System.out.println("-------------");
        System.out.println("YANG BELUM BAYAR!!");
        while(iterator.hasNext()) {
            String nama = iterator.next();
            System.out.println(nama);
        }

        */

        // MAP
        Map<String, String> countryCodes = new HashMap<>();
        countryCodes.put("1", "USA");
        countryCodes.put("44", "UK");
        countryCodes.put("33", "France");
        System.out.println(countryCodes.get("1"));

        countryCodes.put("1", "Indonesia");
        System.out.println(countryCodes.get("1"));
    }

    // get Value ga bisa direct ya, jadi harus di buka satu - satu dulu pake Entry
    public static <K, V> K getKey(Map<K, V> map, V value) {
        for(Map.Entry<K, V> entry: map.entrySet()) {
            if(entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

}
